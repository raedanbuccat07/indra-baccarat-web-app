// src/App.js
import React from 'react';
import './App.css'; // Add your CSS styles here
import BaccaratGame from './components/BaccaratGame';

function App() {
  return (
    <BaccaratGame />
  );
}

export default App;
