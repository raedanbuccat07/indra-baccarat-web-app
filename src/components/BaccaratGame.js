// src/components/BaccaratGame.js
import React, { useState, useEffect, Fragment, useCallback, useRef } from 'react';
import 'animate.css';
import { Howl } from "howler";
import Flip from "../audioclips/Flip.mp3";
import Tada from "../audioclips/Tada.mp3";
import { generateDeck, shuffleDeck } from '../utils/cards';
import { Container, Row, Col, Button, Accordion, Tab, Tabs, CardGroup, Card, Image } from "react-bootstrap";
import ReactCanvasConfetti from "react-canvas-confetti";
import { Scrollbars } from "react-custom-scrollbars-2";
import logoImage from '../images/cardLogo.png';
import Sparkle from 'react-sparkle'

import CardBack from "../images/card_back.png"
import backgroundDefault from '../images/cardbg.jpg';
import defaultBackground from '../images/cardbg.jpg';

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
  zIndex: 999
};

function importAll(r) {
  return r.keys().map(r);
};

// Import card images dynamically (adjust the file paths accordingly)
const cardImages = {
  '2_of_hearts.png': require('../images/22/3.png'),
  '3_of_hearts.png': require('../images/33/3.png'),
  '4_of_hearts.png': require('../images/44/3.png'),
  '5_of_hearts.png': require('../images/55/3.png'),
  '6_of_hearts.png': require('../images/66/3.png'),
  '7_of_hearts.png': require('../images/77/3.png'),
  '8_of_hearts.png': require('../images/88/3.png'),
  '9_of_hearts.png': require('../images/99/3.png'),
  '10_of_hearts.png': require('../images/10/3.png'),
  'J_of_hearts.png': require('../images/00/7.png'),
  'Q_of_hearts.png': require('../images/00/9.png'),
  'K_of_hearts.png': require('../images/00/8.png'),
  'A_of_hearts.png': require('../images/11/3.png'),
  '2_of_spades.png': require('../images/22/4.png'),
  '3_of_spades.png': require('../images/33/4.png'),
  '4_of_spades.png': require('../images/44/4.png'),
  '5_of_spades.png': require('../images/55/4.png'),
  '6_of_spades.png': require('../images/66/4.png'),
  '7_of_spades.png': require('../images/77/4.png'),
  '8_of_spades.png': require('../images/88/4.png'),
  '9_of_spades.png': require('../images/99/4.png'),
  '10_of_spades.png': require('../images/10/4.png'),
  'J_of_spades.png': require('../images/00/0.png'),
  'Q_of_spades.png': require('../images/00/11.png'),
  'K_of_spades.png': require('../images/00/10.png'),
  'A_of_spades.png': require('../images/11/4.png'),
  '2_of_clubs.png': require('../images/22/1.png'),
  '3_of_clubs.png': require('../images/33/1.png'),
  '4_of_clubs.png': require('../images/44/1.png'),
  '5_of_clubs.png': require('../images/55/1.png'),
  '6_of_clubs.png': require('../images/66/1.png'),
  '7_of_clubs.png': require('../images/77/1.png'),
  '8_of_clubs.png': require('../images/88/1.png'),
  '9_of_clubs.png': require('../images/99/1.png'),
  '10_of_clubs.png': require('../images/10/1.png'),
  'J_of_clubs.png': require('../images/00/1.png'),
  'Q_of_clubs.png': require('../images/00/3.png'),
  'K_of_clubs.png': require('../images/00/2.png'),
  'A_of_clubs.png': require('../images/11/1.png'),
  '2_of_diamonds.png': require('../images/22/2.png'),
  '3_of_diamonds.png': require('../images/33/2.png'),
  '4_of_diamonds.png': require('../images/44/2.png'),
  '5_of_diamonds.png': require('../images/55/2.png'),
  '6_of_diamonds.png': require('../images/66/2.png'),
  '7_of_diamonds.png': require('../images/77/2.png'),
  '8_of_diamonds.png': require('../images/88/2.png'),
  '9_of_diamonds.png': require('../images/99/2.png'),
  '10_of_diamonds.png': require('../images/10/2.png'),
  'J_of_diamonds.png': require('../images/00/4.png'),
  'Q_of_diamonds.png': require('../images/00/6.png'),
  'K_of_diamonds.png': require('../images/00/5.png'),
  'A_of_diamonds.png': require('../images/11/2.png'),

  // Repeat the above lines for other suits (Diamonds, Clubs, Spades)
};

const zero = importAll(require.context('../images/00/', false, /\.(png|jpe?g|svg)$/));
const one = importAll(require.context('../images/11/', false, /\.(png|jpe?g|svg)$/));
const two = importAll(require.context('../images/22/', false, /\.(png|jpe?g|svg)$/));
const three = importAll(require.context('../images/33/', false, /\.(png|jpe?g|svg)$/));
const four = importAll(require.context('../images/44/', false, /\.(png|jpe?g|svg)$/));
const five = importAll(require.context('../images/55/', false, /\.(png|jpe?g|svg)$/));
const six = importAll(require.context('../images/66/', false, /\.(png|jpe?g|svg)$/));
const seven = importAll(require.context('../images/77/', false, /\.(png|jpe?g|svg)$/));
const eight = importAll(require.context('../images/88/', false, /\.(png|jpe?g|svg)$/));
const nine = importAll(require.context('../images/99/', false, /\.(png|jpe?g|svg)$/));
const ten = importAll(require.context('../images/10/', false, /\.(png|jpe?g|svg)$/));

function BaccaratGame() {
  const [deck, setDeck] = useState([]);
  const [playerHand, setPlayerHand] = useState([]);
  const [bankerHand, setBankerHand] = useState([]);
  const [winner, setWinner] = useState(null);
  const refAnimationInstance = useRef(null);
  const [playerWins, setPlayerWins] = useState(0);
  const [bankerWins, setBankerWins] = useState(0);
  const [cardsFaceUp, setCardsFaceUp] = useState(false); // To track whether cards are face up

  const [isEmptyDeck, setIsEmptyDeck] = useState(false);

  const [backgroundImage, setBackgroundImage] = useState("");
  const [backgroundBlur, setBackgroundBlur] = useState(0);

  const getInstance = useCallback((instance) => {
    refAnimationInstance.current = instance;
  }, []);

  const makeShot = useCallback((particleRatio, opts) => {
    refAnimationInstance.current &&
      refAnimationInstance.current({
        ...opts,
        origin: { y: 0.7 },
        particleCount: Math.floor(200 * particleRatio),
      });
  }, []);

  const fire = useCallback(() => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55,
    });

    makeShot(0.2, {
      spread: 60,
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  }, [makeShot]);

  useEffect(() => {
    startNewGame();
  }, []);

  const startNewGame = () => {
    const newDeck = shuffleDeck(generateDeck());
    setDeck(newDeck);
    setPlayerHand([]);
    setBankerHand([]);
    setWinner(null);
    setCardsFaceUp(false); // Reset card face-up state
    setIsEmptyDeck(false);

    setPlayerCardFlips([false, false]);
    setBankerCardFlips([false, false]);
  };

  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true,
    });
    sound.play();
  };

  const dealInitialHands = () => {

    if (deck.length === 4) {
      setIsEmptyDeck(true);
    }

    if (deck.length < 4) {
      const newDeck = shuffleDeck(generateDeck());
      setDeck(newDeck);
      setPlayerHand([]);
      setBankerHand([]);
      setWinner(null);
    }

    const playerInitial = [deck.pop(), deck.pop()];
    const bankerInitial = [deck.pop(), deck.pop()];

    setPlayerHand(playerInitial);
    setBankerHand(bankerInitial);

    // Determine the winner after flipping the cards
    const playerTotal = calculateTotal(playerInitial);
    const bankerTotal = calculateTotal(bankerInitial);

    if (playerTotal === 8 || playerTotal === 9 || bankerTotal === 8 || bankerTotal === 9) {
      // Natural win condition
      if (playerTotal === bankerTotal) {
        setWinner('Tie');
      } else if (playerTotal > bankerTotal) {
        setWinner('Player');
        setPlayerWins(playerWins + 1);
      } else {
        setWinner('Banker');
        setBankerWins(bankerWins + 1);
      }
    } else {
      // Continue with standard rules if no natural win
      if (playerTotal > bankerTotal) {
        setWinner('Player');
        setPlayerWins(playerWins + 1);
      } else if (bankerTotal > playerTotal) {
        setWinner('Banker');
        setBankerWins(bankerWins + 1);
      } else {
        setWinner('Tie');
      }
    }

    SoundPlay(Flip);
    setIsEmptyDeck(true);
  };

  const calculateTotal = (hand) => {
    return hand.reduce((total, card) => {
      const value = parseInt(card.rank) || (card.rank === 'A' ? 1 : 0);
      return total + value;
    }, 0) % 10;
  };

  const flipCards = () => {
    SoundPlay(Tada);
    SoundPlay(Flip);
    fire();
    setCardsFaceUp(true);
  };

  const addBlurBackground = () => {
    setBackgroundBlur(backgroundBlur + 1);
  };

  const reduceBlurBackground = () => {
    if (backgroundBlur <= 0) {
      return
    } else {
      setBackgroundBlur(backgroundBlur - 1);
    }
  };

  const removeBlurBackground = () => {
    setBackgroundBlur(0);
  };

  const setDefaultBackground = () => {
    setBackgroundImage(defaultBackground);
  };

  const removeBackground = () => {
    setBackgroundImage();
  };

  // Track the flip state of each card individually
  const [playerCardFlips, setPlayerCardFlips] = useState([false, false]);
  const [bankerCardFlips, setBankerCardFlips] = useState([false, false]);

  // Function to toggle the flip state of a player card
  const togglePlayerCardFlip = (index) => {
    const newFlips = [...playerCardFlips];
    newFlips[index] = !newFlips[index];
    setPlayerCardFlips(newFlips);
    SoundPlay(Flip);

    if (newFlips[0] && newFlips[1] && bankerCardFlips[0] && bankerCardFlips[1]) {
      flipCards();
    }
  };

  // Function to toggle the flip state of a banker card
  const toggleBankerCardFlip = (index) => {
    const newFlips = [...bankerCardFlips];
    newFlips[index] = !newFlips[index];
    setBankerCardFlips(newFlips);
    SoundPlay(Flip);

    if (newFlips[0] && newFlips[1] && playerCardFlips[0] && playerCardFlips[1]) {
      flipCards();
    }
  };

  return (
    <Fragment>
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
      >
        {
          cardsFaceUp ?
            (<Sparkle
              minSize={10}
              maxSize={30}
              flickerSpeed={"fast"}
              overflowPx={-20}
            />)
            :
            (<Sparkle
              minSize={1}
              maxSize={10}
              overflowPx={-20}
            />)
        }

        <div className="lottery-container2 w-100 h-100" style={{ backgroundImage: `url(${backgroundImage ? backgroundImage : backgroundDefault})`, filter: `blur(${backgroundBlur}px)`, WebkitFilter: `blur(${backgroundBlur}px)` }}></div>
        <Container
          fluid
          className="lottery-container h-100 m-0 p-0"
        >

          <Row className="h-35 m-0 p-0 d-flex align-items-center justify-content-center">
            <Col xs={12} className="h-100 logo-box">
              <Image src={logoImage} className="card-logo" />
            </Col>
          </Row>

          <Row className="h-10 m-0 p-3">
            <Col className="d-flex align-items-center justify-content-center col-4 player-txt-bg">
              <h1>Player: {cardsFaceUp || (playerCardFlips[0] && playerCardFlips[1]) ? calculateTotal(playerHand) : '?'}</h1>
            </Col>

            <Col className="d-flex align-items-center justify-content-center col-4">

            </Col>

            <Col className="d-flex align-items-center justify-content-center col-4 player-txt-bg">
              <h1>Banker: {cardsFaceUp || (bankerCardFlips[0] && bankerCardFlips[1]) ? calculateTotal(bankerHand) : '?'}</h1>
            </Col>
          </Row>

          <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />

          <Row className="h-35 m-0 p-0 row-hide2">
            <Col className="d-flex align-items-center justify-content-center col-4 pt-3">
              <CardGroup>
                {playerHand.map((card, index) => (
                  <Card key={index} className={`animate__animated animate__fadeInLeft p-0 m-3 border-0 animate__${index === 1 ? "" : "faster"}`}>
                    <div
                      className={`card ${playerCardFlips[index] || cardsFaceUp ? 'flip' : ''}`}
                      key={index}
                      onClick={() => togglePlayerCardFlip(index)}
                    >
                      <img
                        src={cardsFaceUp || playerCardFlips[index] ? cardImages[`${card.rank}_of_${card.suit.toLowerCase()}.png`] : CardBack}
                        alt={cardsFaceUp || playerCardFlips[index] ? `${card.rank} of ${card.suit}` : 'Card Back'}
                        className={`${cardsFaceUp ? 'face-up' : 'face-down'} card-image-test`}
                      />
                    </div>
                  </Card>
                ))}
              </CardGroup>
            </Col>

            <Col className="d-flex align-items-center justify-content-center col-4 pt-3">

            </Col>

            <Col className="d-flex align-items-center justify-content-center col-4 pt-3">
              <CardGroup>
                {bankerHand.map((card, index) => (
                  <Card key={index} className={`animate__animated animate__fadeInRight p-0 m-3 border-0 animate__${index === 0 ? "" : "faster"}`}>
                    <div
                      className={`card ${bankerCardFlips[index] || cardsFaceUp ? 'flip' : ''}`}
                      key={index}
                      onClick={() => toggleBankerCardFlip(index)}
                    >
                      <img
                        src={cardsFaceUp || bankerCardFlips[index] ? cardImages[`${card.rank}_of_${card.suit.toLowerCase()}.png`] : CardBack}
                        alt={cardsFaceUp || bankerCardFlips[index] ? `${card.rank} of ${card.suit}` : 'Card Back'}
                        className={`${cardsFaceUp ? 'face-up' : 'face-down'} card-image-test`}
                      />
                    </div>
                  </Card>
                ))}
              </CardGroup>
            </Col>
          </Row>

          <Row className="h-10 m-0 p-0 row-hide">
            {
              cardsFaceUp ?
                <Col className={`col-12 d-flex justify-content-center align-items-start m-0 p-0`}>
                  <pre className="pt-3 p-0 m-0 ">
                    <h1 className="first-data text-center animate__animated animate__backInDown animate__faster">{winner && <p>WINNER: {winner}</p>}</h1>
                  </pre>
                </Col>
                :
                ""
            }
          </Row>

          <Row className="h-10 m-0 p-0">
            <Col className={`col-12 d-flex justify-content-center align-items-center`}>

              {isEmptyDeck && cardsFaceUp ?
                <Button
                  className={`card-button text-uppercase flip-button-color-red`}
                  size="sm"
                  variant="light"
                  onClick={startNewGame}

                  active
                >Reset</Button>
                :

                !isEmptyDeck && !cardsFaceUp ?
                  <Button
                    className={`card-button text-uppercase flip-button-color-blue`}
                    size="sm"
                    variant="light"
                    onClick={dealInitialHands}

                    active
                  >
                    Draw
                  </Button>
                  :
                  <Button
                    className={`card-button text-uppercase flip-button-color-blue`}
                    size="sm"
                    variant="light"
                    onClick={flipCards}

                    active
                  >
                    Flip Cards
                  </Button>

              }
            </Col>
          </Row>

          <Accordion>
            <Accordion.Item eventKey="0">
              <Accordion.Header>OPTIONS (CLICK ME)</Accordion.Header>
              <Accordion.Body className="accordion-bg">
                <Tabs defaultActiveKey="file" className="mb-3">
                  <Tab eventKey="file" title="Upload File">
                    <Row>
                      <Col className="text-start ">
                        <form>
                          <label
                            htmlFor="fileInput"
                            style={{
                              cursor: "pointer",
                              background: "grey",
                              padding: "5px",
                              color: "white",
                            }}
                          >
                            Set Background Image
                          </label>
                          <input
                            className="d-none"
                            style={{ visibility: "hidden" }}
                            type="file"
                            name="fileInput"
                            id="fileInput"
                          />
                        </form>
                      </Col>
                      <Col className="text-start">
                        <Button variant="info" onClick={() => setDefaultBackground()}>Add Default Background</Button>
                      </Col>
                      <Col className="text-start">
                        <Button variant="dark" onClick={() => removeBackground()}>Remove Background</Button>
                      </Col>
                      <Col className="text-start">
                        <Button variant="success" onClick={() => addBlurBackground()}>Add Blur</Button>
                        <Button variant="primary" onClick={() => reduceBlurBackground()}>Reduce Blur</Button>
                        <Button variant="danger" onClick={() => removeBlurBackground()}>Remove Blur</Button>
                      </Col>
                    </Row>
                  </Tab>
                  <Tab eventKey="loadCards" title="Load Cards">
                    <Row className="w-100 h-100 p-0 m-0">
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          <Card>
                            <Card.Img variant="top" src={CardBack} />
                          </Card>
                          {
                            zero.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            one.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            two.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            three.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            four.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            five.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            six.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            seven.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            eight.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            nine.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                      <Col xs={12}>
                        <CardGroup className="h-100 w-100">
                          {
                            ten.map((data, index) => {
                              return (
                                <Card key={index + data}>
                                  <Card.Img variant="top" src={data} />
                                </Card>
                              );
                            })
                          }
                        </CardGroup>
                      </Col>
                    </Row>
                  </Tab>
                </Tabs>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Container>
      </Scrollbars>
    </Fragment>
  );
}

export default BaccaratGame;